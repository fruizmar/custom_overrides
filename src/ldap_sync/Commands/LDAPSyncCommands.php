<?php

namespace Drupal\custom_overrides\ldap_sync\Commands;

use Drupal;
use Drupal\ldap_sync\Commands\LDAPSyncCommands as LDAPSyncCommandsOriginal;
use Drush\Drush;
use Drupal\custom_overrides\ldap_sync\DrushCommand\LdapSyncCommand;

/**
 * A drush command file.
 *
 * @package Drupal\custom_overrides\ldap_sync\Commands
 */
class LDAPSyncCommands extends LDAPSyncCommandsOriginal {
  
  /**
  * Sync data with LDAP.
  *    
  * @param string $origin
  * @command ldap_sync:sync
  * @aliases lsync ldap-sync
  * @bootstrap database
  * @option all Retrieve all objects from IdMC.
  */
  function drush_ldap_sync(string $origin) {
    $ldapSyncCommand = LdapSyncCommand::create(Drupal::getContainer());
  
    if ($ldapSyncCommand->execute($origin)) {
      Drush::output()->writeln('LDAP synchronization finished.');
      Drupal::logger("LDAP")->info('LDAP synchronization finished.');
    }
    else {
      Drush::output()->writeln('LDAP synchronization error or already running.');
      Drupal::logger("LDAP")->warning('LDAP synchronization error or already running.');
    }
  }
  
  
}