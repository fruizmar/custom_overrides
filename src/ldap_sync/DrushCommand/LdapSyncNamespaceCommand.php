<?php
declare(strict_types=1);

namespace Drupal\custom_overrides\ldap_sync\DrushCommand;

use Drupal\ldap_sync\DrushCommand\LdapSyncNamespaceCommand as LdapSyncNamespaceCommandOriginal;

/**
 * Checks if LDAP sync must be executed for an origin.
 */
class LdapSyncNamespaceCommand extends LdapSyncNamespaceCommandOriginal {  

}
