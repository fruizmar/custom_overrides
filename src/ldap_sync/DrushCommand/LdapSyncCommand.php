<?php

declare(strict_types=1);

namespace Drupal\custom_overrides\ldap_sync\DrushCommand;

use Drupal\custom_overrides\ldap_sync\RorEventsBatch\LdapBatch;
use Drupal\idmc\Util\State\LdapSyncState;
use Drupal\ldap_sync\DrushCommand\LdapSyncCommand as LdapSyncCommandOriginal;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Checks if LDAP sync must be executed for an origin.
 */
class LdapSyncCommand extends LdapSyncCommandOriginal
{

    public static function create(ContainerInterface $container): LdapSyncCommand
    {
        return new static(
            $container->get('database'),
            $container->get('logger.factory')
        );
    }

    public function execute(string $originName): bool
    {
        $this->state->setOrigin($originName);
        $currentStage = $this->state->getStage();

        if (
            $currentStage == LdapSyncState::STAGE_WAITING ||
            $currentStage == LdapSyncState::STAGE_STOPPED
        ) {

            return $this->syncOrigin($originName);
        }

        return FALSE;
    }

    private function syncOrigin(string $originName): bool
    {
        $result = FALSE;
        $this->state->setLastSyncStart(time());
        $this->state->setStage(LdapSyncState::STAGE_RUNNING);
        $this->state->save();

        try {
            $rorEventsBatch = new LdapBatch();
            $result = $rorEventsBatch->updateRorEventsData($originName);
        } finally {
            $this->state->setLastSyncFinish(time());
            $this->state->setStage(LdapSyncState::STAGE_STOPPED);
        }

        return $result;
    }
}
