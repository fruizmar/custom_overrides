<?php

declare(strict_types=1);

namespace Drupal\custom_overrides\ldap_sync\RorEventsBatch;

use Drupal\custom_overrides\ldap_sync\RorEventsBatch\providers\RorEventsProviderBase;
use Drupal\dal\Util\RepositoryFactory;
use Drupal\ldap_sync\RorEventsBatch\LdapBatch as LdapBatchOriginal;

use Drupal\proxy_ror\Util\NamespaceUtils;
use Drupal\dal\Domain\Traits\StateInterface;
use Drupal\dal\Domain\RorObjectBase;


class LdapBatch extends LdapBatchOriginal
{
    private $eventCount = [];
    public function updateRorEventsData(string $origin): bool
    {
        $this->eventCount = [];
        $updateStart      = time();

        try {
            $rorEventsProvider = RorEventsProviderFactory::create($origin);
            $rorEventsProvider->connect();

            try {
                foreach (NamespaceUtils::getNamespaces() as $namespace) {
                    $this->updateRorEventsNamespace($namespace, $rorEventsProvider);
                }
            } finally {
                $rorEventsProvider->disconnect();
            }

            $updateEnd = time();

            if (!empty($this->eventCount)) {
                $this->logger->info('Data download from LDAP completed in @total_time for origin @origin.', [
                    '@domain_id' => $this->domain_id,
                    '@origin' => $origin,
                    '@update_start' => date('Y-m-d H:i:s', $updateStart),
                    '@update_end' => date('Y-m-d H:i:s', $updateEnd),
                    '@total_time' => $this->totalTime($updateStart, $updateEnd),
                ] + $this->eventCount);
            }

            return TRUE;
        } catch (\Throwable $t) {
            $this->logger->error('Error updating events data for origin @origin.', [
                '@domain_id' => $this->domain_id,
                '@origin' => $origin,
                '@message' => $t->getMessage(),
                '@total_time' => $this->totalTime($updateStart, time()),
            ]);

            return FALSE;
        }
    }


    public function updateRorEventsNamespace(
        string $namespace,
        RorEventsProviderBase $rorEventsProvider
    ): bool {

        $updateStart = time();
        $disconnect  = FALSE;

        try {
            $repository       = RepositoryFactory::create($namespace);
            $outdated_records = $repository->getOutdatedRecords($rorEventsProvider->getOriginName());

            if ($outdated_records) {
                $this->logger->info('Found @event_outdated_count @namespace to download from LDAP @origin.', [
                    '@event_outdated_count' => count($outdated_records),
                    '@namespace' => $namespace,
                    '@origin' => $rorEventsProvider->getOriginName(),
                ]);

                if (!$rorEventsProvider->isConnected()) {
                    $rorEventsProvider->connect();
                    $disconnect = TRUE;
                }

                foreach ($outdated_records as $domain_object) {
                    /** @var RorObjectBase $domain_object */

                    if (!$rorEventsProvider->isConnected()) {
                        throw new \Exception('Lost connection with LDAP');
                    }

                    try {
                        $eventUpdated = $rorEventsProvider->updateEvent($domain_object);
                    } catch (\Throwable $t) {
                        $this->logEventError($t, $rorEventsProvider->getOriginName(), $namespace);
                        $eventUpdated = FALSE;

                        if (
                            !$rorEventsProvider->isConnected() ||
                            strpos($t->getMessage(), 'MySQL server has gone away') !== FALSE
                        ) {

                            throw $t;
                        }
                    }

                    if ($eventUpdated) {
                        $this->eventCount['@event_updated_count']        += 1;
                        $this->eventCount["@{$namespace}_updated_count"] += 1;
                    } else {
                        $domain_object->setState(StateInterface::STATE_ERROR);
                        $repository->save($domain_object);

                        $this->eventCount['@event_error_count']        += 1;
                        $this->eventCount["@{$namespace}_error_count"] += 1;
                    }

                    $count = $this->eventCount["@{$namespace}_updated_count"] ?? 0;
                    if ($count && $count % 1000 == 0) {
                        $this->logger->info('@count @namespace downloaded from LDAP @origin at @speed objects/second...', [
                            '@count' => $count,
                            '@namespace' => $namespace,
                            '@origin' => $rorEventsProvider->getOriginName(),
                            '@speed' => number_format($count / (time() - $updateStart), 2),
                        ]);
                    }
                }

                $updateEnd = time();

                $this->logger->info('Download for @namespace in @origin took @total_time.', [
                    '@domain_id' => $this->domain_id,
                    '@origin' => $rorEventsProvider->getOriginName(),
                    '@namespace' => $namespace,
                    '@update_start' => date('Y-m-d H:i:s', $updateStart),
                    '@update_end' => date('Y-m-d H:i:s', $updateEnd),
                    '@total_time' => $this->totalTime($updateStart, $updateEnd),
                ] + $this->eventCount);

                return TRUE;
            }
        } catch (\Throwable $t) {
            $this->logEventError($t, $rorEventsProvider->getOriginName(), $namespace);
        } finally {
            if ($disconnect) {
                $rorEventsProvider->disconnect();
            }
        }

        return FALSE;
    }
}
