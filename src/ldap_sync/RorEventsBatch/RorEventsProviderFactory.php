<?php
declare(strict_types=1);

namespace Drupal\custom_overrides\ldap_sync\RorEventsBatch;

use Drupal\custom_overrides\ldap_sync\RorEventsBatch\providers\RorEventsProviderBase;
use Drupal\cutom_overrides\ldap_sync\RorEventsBatch\providers\RorBurgundyProvider;
use Drupal\ldap_sync\RorEventsBatch\RorEventsProviderFactory as RorEventsProviderFactoryOriginal;

class RorEventsProviderFactory extends RorEventsProviderFactoryOriginal {

  public static function create(string $origin): RorEventsProviderBase {
    switch ($origin) {
      case RorBurgundyProvider::ORIGIN_NAME:
        return new RorBurgundyProvider($origin);

      default:
        throw new \Exception("Origin $origin not implemented");
    }
  }

}
