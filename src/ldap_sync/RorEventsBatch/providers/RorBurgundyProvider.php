<?php

declare(strict_types=1);

namespace Drupal\cutom_overrides\ldap_sync\RorEventsBatch\providers;

use Drupal\custom_overrides\ldap_sync\RorEventsBatch\providers\RorEventsProviderBase;
use Drupal\custom_overrides\ldap_sync\RorEventsBatch\updaters\ror_brg\RorBurgundyProfessional;
use Drupal\dal\Domain\Organization;
use Drupal\dal\Domain\Professional;
use Drupal\ldap_sync\RorEventsBatch\providers\RorBurgundyProvider as RorBurgundyProviderOriginal;
use Drupal\ldap_sync\RorEventsBatch\updaters\ror_brg\RorBurgundyOrganization;
use Drupal\ldap_sync\RorEventsBatch\updaters\ror_brg\RorBurgundyStructure;
use Drupal\ldap_sync\RorEventsBatch\updaters\ror_brg\RorBurgundyUnit;
use Drupal\dal\Domain\RorObjectBase;
use Drupal\dal\Domain\Structure;
use Drupal\dal\Domain\Unit;

class RorBurgundyProvider extends RorEventsProviderBase
{
    const ORIGIN_NAME = 'ROR_GUY';

    /**
     * @var RorBurgundyStructure
     */
    private $structureUpdater;

    /**
     * @var RorBurgundyOrganization
     */
    private $organizationUpdater;

    /**
     * @var RorBurgundyUnit
     */
    private $unitUpdater;

    /**
     * @var RorBurgundyProfessional
     */
    private $professionalUpdater;

    /**
     * @var string
     */
    private $baseDnChain;

    public function __construct(string $origin)
    {
        parent::__construct($origin);

        $this->structureUpdater    = new RorBurgundyStructure();
        $this->organizationUpdater = new RorBurgundyOrganization();
        $this->unitUpdater         = new RorBurgundyUnit();
        $this->professionalUpdater = new RorBurgundyProfessional();

        $this->baseDnChain = $this->rorEventsParam->getBaseDnChain();
    }

    /**
     * Update the object from the LDAP data
     *
     * @param RorObjectBase $object
     * @return bool FALSE if not found in LDAP.
     *
     * @throws Exception On other errors.
     */
    public function updateEvent(RorObjectBase $object): bool
    {
        if ($object instanceof Structure) {
            $ldapUpdater = $this->structureUpdater;
        } elseif ($object instanceof Organization) {
            $ldapUpdater = $this->organizationUpdater;
        } elseif ($object instanceof Unit) {
            $ldapUpdater = $this->unitUpdater;
        } elseif ($object instanceof Professional) {
            $ldapUpdater = $this->professionalUpdater;
        } else {
            throw new \Exception('Unrecognized object type');
        }

        $ldapUpdater->setObject($object);

        if ($object instanceof Unit) {
            $newData = $this->downloadUnitFromLdap(
                $ldapUpdater->getBaseDn($this->baseDnChain),
                $ldapUpdater->getFilter()
            );
        } else {
            $newData = $this->downloadFromLdap(
                $ldapUpdater->getBaseDn($this->baseDnChain),
                $ldapUpdater->getFilter()
            );
        }

        if ($newData) {
            $ldapUpdater->updateObject($newData);
        }

        return (bool) $newData;
    }
}
