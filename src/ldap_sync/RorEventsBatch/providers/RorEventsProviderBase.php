<?php


namespace Drupal\custom_overrides\ldap_sync\RorEventsBatch\providers;

use Drupal;
use Drupal\dal\Domain\RorObjectBase;
use Drupal\ldap_sync\RorEventsBatch\providers\RorEventsProviderBase as RorEventsProviderBaseOriginal;
use Drupal\Core\Site\Settings;
use Drupal\dal\Repository\RorEventsParamRepository;

abstract class RorEventsProviderBase extends RorEventsProviderBaseOriginal
{
    abstract public function updateEvent(RorObjectBase $object): bool;
    public function __construct(string $origin)
    {
        $rorEventsParamRepository = RorEventsParamRepository::create();

        $this->logger         = Drupal::logger('ldap_sync');
        $this->domainId       = Settings::get('domain_id');
        $this->origin         = $origin;
        $this->rorEventsParam = $rorEventsParamRepository->findByOrigin($origin);
    }

    public function getOriginName(): string
    {
        return (string) $this->origin;
    }

    public function connect()
    {
        try {
            $hostname = $this->rorEventsParam->getHostname();
            $port     = $this->rorEventsParam->getPort();
            $login    = $this->rorEventsParam->getLoginChain();
            $password = $this->rorEventsParam->getPassword();

            $conn = ldap_connect("ldaps://{$hostname}:{$port}");
            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_NETWORK_TIMEOUT, 10);

            if (!ldap_bind($conn, $login, $password)) {
                throw new \Exception(ldap_error($conn));
            }

            $this->conn = $conn;
        } catch (\Throwable $t) {
            $this->conn = NULL;
            throw new \Exception($t->getMessage() . ": {$hostname}:{$port}");
        }
    }

    public function disconnect()
    {
        if ($this->conn) {
            ldap_unbind($this->conn);
            $this->conn = NULL;
        }
    }

    public function isConnected(): bool
    {
        return (bool) $this->conn;
    }

    protected function downloadFromLdap(string $baseDn, string $filter): array
    {
        try {
            $search = ldap_list($this->conn, $baseDn, $filter);

            if ($search === FALSE) {
                throw new \Exception('Error searching in LDAP: ' . ldap_error($this->conn) . '.');
            }

            $result = ldap_get_entries($this->conn, $search);

            if ($result === FALSE) {
                throw new \Exception('Error searching in LDAP: ' . ldap_error($this->conn) . '.');
            } elseif (isset($result['count']) && $result['count'] > 0) {
                return $result[0];
            }

            throw new \Exception('Search in LDAP returned no results.');
        } catch (\Throwable $t) {
            $this->logger->error($t->getMessage(), [
                '@domain_id' => $this->domainId,
                '@origin' => $this->origin,
                '@baseDn' => $baseDn,
                '@filter' => $filter,
            ]);
        }

        return [];
    }
}
