<?php

declare(strict_types=1);

namespace Drupal\custom_overrides\ldap_sync\RorEventsBatch\updaters\ror_brg;

use Drupal\ldap_sync\RorEventsBatch\updaters\ror_brg\RorBurgundyProfessional as RorBurgundyProfessionalOriginal;

class RorBurgundyProfessional extends RorBurgundyProfessionalOriginal
{
    public function updateObject(array $newData): void
    {
        if (isset($newData['specialiteprofessionnel']['count'])) {
            $newData['specialite']  = $newData['specialiteprofessionnel'];
        }
        parent::updateObject($newData);
    }
}
