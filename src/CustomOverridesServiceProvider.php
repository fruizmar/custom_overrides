<?php

namespace Drupal\custom_overrides;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal;
use Drush\Drush;

class CustomOverridesServiceProvider extends ServiceProviderBase implements ServiceProviderInterface
{
  public function alter(ContainerBuilder $container)
  {  
    Drush::output()->writeln('alter');
    if ($container->hasDefinition('ldap_sync.commands')) {      
      $definition = $container->getDefinition('ldap_sync.commands');
      $definition->setClass('Drupal\custom_overrides\ldap_sync\Commands\LDAPSyncCommands');
    }
  }

}
